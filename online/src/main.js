
/**
 *
 * @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 * Copyright (C) 2014  Loic J. Duros
 *
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 *
 */

/**
 * Rep is a superclass for defining an object that is a
 * JavaScript representative for a DOM element.
 */
class Rep {
    constructor () {
        this.element = null;
        this.children = []; // all children of representative
    };
    appendChild(newChild) {
        this.children.push(newChild);
        this.element.appendChild(newChild.element);
    }
    draw() {
        this.reDraw();
    };
    reDraw() {
        this.reColor();
    };
    reColor() {
        this.children.forEach(function (child) { child.reColor(); });
    };
}

class App extends Rep {
    constructor() {
        super();
        this.element = document.body;
        this.initStyle();
        this.appendChild(new Nav());
    }
    initStyle() {
        this.element.style.fontFamily = "sans-serif";
        this.element.style.maxWidth = "1024px";
        this.element.style.marginLeft = "auto";
        this.element.style.marginRight = "auto";
        super.draw();
    }
    reColor() {
        this.element.style.backgroundColor = "#000000";
        this.element.style.color = "#ffffff";
        super.reColor();
    }
}

class Nav extends Rep {
    constructor() {
        super();
        this.element = document.createElement("header");
        this.element.onmouseover = () => {
            this.element.style.borderRadius = "20px";
        };
        this.element.onmouseout = () => {
            this.element.style.borderRadius = "10px";
        };
        this.initStyle();
    }
    initStyle() {
        this.element.style.transitionDuration = "250ms";
        this.element.style.transitionTimingFunction = "ease-out";
        this.element.style.position = "relative";
//        this.element.style.minHeight = "50px";
        this.element.style.borderRadius = "10px";
        this.element.style.padding = "12px";
        this.element.style.paddingLeft = "16px";
        this.element.style.paddingBottom = "16px";
        // this.element.innerText = "Arnaldur.be";
        this.appendChild(new Link("Arnaldur"));
        this.appendChild(new Span("."));
        this.appendChild(new Link("be"));
        super.draw();
    }
    reColor() {
        this.element.style.backgroundColor = "#292929";
        super.reColor();
    }
}

class Link extends Rep {
    constructor(text, to="") {
        super();
        this.element = document.createElement("a");
        this.element.innerText = text;

        this.element.href = to;
        this.element.style.transitionDuration = "250ms";
        this.element.onmouseover = () => {
            this.element.style.color = "#cccccc";
            this.reColor();
        };
        this.element.onmouseout = () => {
            this.element.style.color = "#ffffff";
        };
        this.element.onmousedown = () => {
            alert("HAHA UR HAZ NOW WIRUS AND MINE BITCOINZ 4 ME");
        };
        this.initStyle();
    }
    initStyle() {
        this.element.style.color = "#ffffff";
    }
}

class Span extends Rep {
    constructor(text) {
        super();
        this.element = document.createElement("span");
        this.element.innerText = text;
    }
}

// class NavHead extends Rep {
//     constructor() {
//         super();
//         this.element = document.createElement()
//     }
// }

new App();

