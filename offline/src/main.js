
/**
 *
 * @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 * Copyright (C) 2018  Arnaldur Bjarnason
 *
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 *
 */

/**
 * Rep is a superclass for defining an object that is a
 * JavaScript representative for a DOM element.
 */
class Rep {
    constructor(tag = null) {
        if (tag !== null) {
            this.element = document.createElement(tag);
            this.initStyle();
        }
        this.children = []; // all children of representative
    };
    appendChild(newChild) {
        this.children.push(newChild);
        this.element.appendChild(newChild.element);
    }
    addStyle(style) {
        Object.assign(this.element.style, style);
    }
    initStyle() {
    };
    applyToChildren(method) {
        this.children.forEach(function (child) { child.method(); child.applyToChildren(method); });
    };
}

class RepPadded extends Rep {
    constructor(tag) {
        super(tag);
    }
    initStyle() {
        super.initStyle();
        this.addStyle({
            transitionDuration: "250ms",
            transitionTimingFunction: "ease-out",
            position: "relative",
            margin: "20px",
            borderRadius: "10px",
            padding: "16px",
            paddingTop: "14px",
            paddingBottom: "14px",
            backgroundColor: "#292929",
        });
        this.element.onmouseover = () => {
            this.element.style.borderRadius = "20px";
        };
        this.element.onmouseout = () => {
            this.element.style.borderRadius = "10px";
        };
    }
}

class App extends Rep {
    constructor() {
        super(null);
        this.element = document.body;
        this.initStyle();
        this.appendChild(new Nav());
        this.appendChild(new Article());
    }
    initStyle() {
        this.addStyle({
            fontFamily: "sans-serif",
            maxWidth: "1024px",
            marginLeft: "auto",
            marginRight: "auto",
            backgroundColor: "#000000",
            color: "#d0d0d0",
        });
    }
}

class Nav extends RepPadded {
    constructor() {
        super("header");
        this.appendChild(new Link("Arnaldur"));
        this.appendChild(new Span("."));
        this.appendChild(new Link("be", "/explaining/habitual/be"));
    }
    initStyle() {
        super.initStyle();
    }
}

class Link extends Rep {
    constructor(text, to="") {
        super("a");
        this.element.innerText = text;
        this.element.href = "javascript:";
        this.element.onclick = () => {
            const state = { url: to };
            history.pushState(state, "", to);
        };
    }
    initStyle() {
        this.element.style.transitionDuration = "250ms";
        this.element.onmouseover = () => {
            this.element.style.color = "#cccccc";
        };
        this.element.onmouseout = () => {
            this.element.style.color = "#ffffff";
        };
        this.element.style.color = "#ffffff";
    }
}

class Span extends Rep {
    constructor(text) {
        super("span");
        this.element.innerText = text;
    }
}

class Article extends RepPadded {
    constructor() {
        super("article");
        this.element.innerText = `Eos velit dolorum doloremque accusantium. Et nesciunt atque sint porro reprehenderit. In enim nisi non reiciendis consequatur. Officiis cumque fugiat et rerum. Maiores est odit maiores et ab minus.

Cumque rerum minima necessitatibus id et earum. Ut beatae sit aspernatur. Delectus minus eligendi vel voluptatem qui minima quibusdam. Amet assumenda numquam officia corporis tenetur.

Inventore sunt et necessitatibus ratione reiciendis modi laborum veniam. Labore ut molestiae accusantium molestias repellendus. Aperiam nulla dolorem autem repellat enim consequuntur. Placeat dolorum eum animi sed enim sint ut.

Voluptatem laboriosam architecto voluptas eos et ut temporibus et. Non et minima et l;askdjf;lakjdsfhic libero tenetur. Occaecati et placeat et error voluptate molestias excepturi. Dolorem et ab blanditiis. Ex voluptatem magni id provident blanditiis atque quo veritatis. Debitis qui natus qui et labore dolorem eum quia.

Modi nihil consequuntur odio est nulla quia at. Harum dignissimos magni veritatis in aliquam ut. Cumque dolores veniam et distinctio nobis. Modi porro totam omnis modi enim omnis harum. Vel non molestiae repellat suscipit.
This is An article, don't catch me slippin' no! Look what I'm whipping tho`;
    }
}

// class NavHead extends Rep {
//     constructor() {
//         super();
//         this.element = document.createElement()
//     }
// }

new App();

